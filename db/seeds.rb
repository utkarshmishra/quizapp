# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
questions = Question.create([{ content: 'What' },
                            { content: 'Why' },
                            { content: 'How' },
                            { content: 'When' },
                            { content: 'Where' }])
questions.each do |q|
    q.choices<<Choice.create(name: 'alpha')
    q.choices<<Choice.create(name: 'beta')
    q.choices<<Choice.create(name: 'gamma')
end