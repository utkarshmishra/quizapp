class CreateSubmissionSelections < ActiveRecord::Migration[5.1]
  def change
    create_table :submission_selections do |t|
      t.references :submission, foreign_key: true
      t.references :choice, foreign_key: true

      t.timestamps
    end
  end
end
