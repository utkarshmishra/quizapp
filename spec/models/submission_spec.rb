require 'rails_helper'

RSpec.describe Submission, type: :model do
  it { should have_many(:submission_selections) }
end
