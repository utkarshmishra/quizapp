require 'rails_helper'

RSpec.describe SubmissionSelection, type: :model do
  it { should belong_to(:choice) }
  it { should belong_to(:submission) }
end
