require 'rails_helper'

RSpec.describe Question, type: :model do
  
  before(:all) do
    @question = FactoryGirl.create :question
    @question2 = FactoryGirl.create :question
  end

  it { should have_many(:choices) }

  describe "valid_choice?" do
    it 'returns true for valid choice' do
      expect(@question.valid_choice? @question.choices.first.id).to be true
    end
    it 'returns false for invalid choice' do
      expect(@question.valid_choice? -1).to be false
    end
  end

  describe "get_next_id" do
    it 'returns next question id if not last' do
      expect(Question.get_next_id @question.id).to eq(@question2.id) 
    end
    it 'returns nil if last' do
      expect(Question.get_next_id @question2.id).to eq(nil) 
    end
  end
end
