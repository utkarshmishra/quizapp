FactoryGirl.define do
  factory :submission_selection do
    association :submission
    association :choice
  end
end
