FactoryGirl.define do
  factory :question do
    content "MyText"
    after(:create) do |q|
      FactoryGirl.create_list(:choice, 3, question: q)
    end
  end
end
