class SubmissionSelection < ApplicationRecord
  belongs_to :submission
  belongs_to :choice

  def self.all_valid
    #TODO add a flag for completed submissions for scalability
    where(submission_id: Submission.completed_ids)
  end
end
