class Submission < ApplicationRecord
  has_many :submission_selections

  def self.completed_ids
    total_questions = Question.count
    SubmissionSelection.group(:submission_id).count.
    select{|k,v| v==total_questions}.keys
  end
end
