class Question < ApplicationRecord
  has_many :choices

  def valid_choice? choice_id
    choice_ids.include? choice_id
  end

  def self.get_next_id current_id
    get_next(current_id).pluck(:id).first
  end

  def self.get_next current_id
    where('id>?', current_id).limit(1)
  end

  def self.valid_choice? choice_id, question_id
    find(question_id).valid_choice? choice_id
  end
end
