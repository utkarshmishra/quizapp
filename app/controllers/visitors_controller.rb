class VisitorsController < ApplicationController
  def index
    if session[:submission].present? && session[:submission]['finished']
      redirect_to submissions_path
    elsif session[:submission].present?
      redirect_to question_path(Question.find(session[:submission]['question_id']))
    end
  end
end
