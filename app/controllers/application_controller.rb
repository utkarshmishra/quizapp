class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  def redirect_next_question
    if session[:submission]['finished']
      redirect_to submissions_path
    else
      redirect_to question_path(Question.find(session[:submission]['question_id']))
    end
  end
end
