class SubmissionSelectionsController < ApplicationController
  before_action :question_order

  def create
    submission_selection = SubmissionSelection.create(create_params)
    redirect_next_question
  end

  private

  def create_params
    params.require(:submission_selection).permit(:choice_id, :submission_id)
  end

  def question_order
    if Question.valid_choice?(create_params[:choice_id].to_i, session[:submission]['question_id'])
      set_session(Question.get_next_id(session[:submission]['question_id']))
    else
      render plain: 'Invalid Order', status: 400 
    end
  end

  def set_session question_id
    if question_id.nil? 
      session[:submission]['finished']=true
    else
      session[:submission]['question_id']=question_id
    end
  end
end
