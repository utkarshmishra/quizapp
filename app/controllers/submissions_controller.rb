class SubmissionsController < ApplicationController
  def create
    binding.pry
    submission = Submission.create(create_params)
    session[:submission] = {id: submission.id, 
                question_id: Question.first.id,
                guest_name: submission.guest_name,
                finished: false}
    redirect_to question_path(Question.first)
  end

  def index
    @valid_submissions = SubmissionSelection.all_valid.
    group(:choice_id).count 
  end

  private

  def create_params
    params.require(:submission).permit(:guest_name)
  end
end
