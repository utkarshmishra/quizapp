class QuestionsController < ApplicationController
  before_action :validate_order
  def show
    @question = Question.find(params[:id])
  end

  private 
  def validate_order
    if session[:submission]['question_id']!=params[:id].to_i
      render plain: 'access denied', status: 400
    end
  end
end
