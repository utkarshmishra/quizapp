Rails.application.routes.draw do
  root to: 'visitors#index'
  resources :questions, only: [:show] 
  resources :submissions, only: [:create, :index] 
  resource :submission_selections, only: [:create] #TODO nest under submission once auth is done
end
